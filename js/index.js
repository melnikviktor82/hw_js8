"use strict";

// ## Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)

// Це модель html-документа, в якій кожний елемент документа (тег, текст, коментар) є об'єктом в дереві документа, 
// де є батьківські і дочірні елементи, і всі ці об'єкти доступні за допомогою JavaScript.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

// innerHTML показує нам весь внутрішній html елемента, innerText - лише текст.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// За допомогою методів:
// document.getElementById,
// document.getElementsByTagName,
// document.getElementsByClassName,
// document.querySelector,
// document.querySelectorAll.
// (querySelector і querySelectorAll нові і зручні, можно шукати по id, tag, class)


// ## Завдання

// Код для завдань лежить в папці project.

// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// 3) Встановіть в якості контента елемента з класом testParagraph наступний параграф - <p>This is a paragraph<p/>

// 4) Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.  

let paragraph = document.querySelectorAll("p");
for (const el of paragraph) {
    el.style.backgroundColor = "#ff0000"
};

let optionsList = document.querySelector("#optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
console.log(optionsList.hasChildNodes());

let optionsListChilds = optionsList.childNodes;
for (const el of optionsListChilds) {
    console.log(el.nodeName);
    console.log(el.nodeType);
};

let testParagraph = document.querySelector("#testParagraph");
testParagraph.innerHTML = "<p>This is a paragraph<p/>";
console.log(testParagraph);

let mainHeader = document.querySelector(".main-header");
let mainHeaderLi = mainHeader.querySelectorAll("li");
for (const el of mainHeaderLi) {
    el.setAttribute("class", "nav-item");
    console.log(el);
};

let sectionTitles = document.querySelectorAll(".section-title");
for (const el of sectionTitles) {
    el.classList.remove("section-title");
};
